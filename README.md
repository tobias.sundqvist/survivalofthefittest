# About
## Survival of the fittest
This is a game in which you play against animals and try to eat more grass than the animals.
The animals are using reinforcemnet learning to find the grass and needs to be trained in order to improve.
At the moment you cannot win since you only can move around as a player and see the animal eat.
Ideas are to also add multiple animals with different behaviour in order to find the best animal, thats why the name of the game is survival of the fittest

made by Tobias Sundqvist (Qvistigt)

# Pre requirement to run
- Python 3.6.1 needs to be installed
- Install pygame (Debian: python3 -m pip install -U pygame --user)
- Install numpy (Debian: pip install numpy)
- Install keras (Debian: pip install keras)
- Install tensorflow (Debian: pip install tensorflow)

# How to run
Program can be run with arguments, use --help or -h to see options
- python run_survival.py

## Training agents
In order to first train the agent then skip the visual part by adding the -r0 flag, this will skip the render of the screen which speeds up the training, this will also save the weights of the training so the agent can start a game with experience from previous sessions
- python run_survival.py -r0

## Loading experience for agents
The agents can load previous experience if the flag -l1 is provided:
- python run_survival.py -l1

# How to test
Perform unit test by the following command:
- python run_test.py

In order to prevent that commits contains faulty code, the unit tests can also be run before any git commit by adding a git pre-commit hook.
Simply copy the file pre-commit to the ./git/hook directory and make it an executable (chmod 777 .git/hooks/pre-commit). In this way all unit tests will run before any commit and if they fail then you are now allowed to commit your code.

# File structure
## Software classes:
- run_survival.py: main class that setups and starts the game
- survival_game.py: class that holds all the classes used by the game
- ddqn_agent.py: class that has a deep q neural network agent, can be used for anything in the game

## Test classes:
- run_test.py: class used for unit testing
- pre-commit: git hook file that can be used to trigger run of all tests before an commit is made.

## Documents:
- README.md: this file

## Gitfiles:
- .gitignore: ignores files that should not be commited
- pre-commit: git hook file to be put in .git/hooks in order to run all unittests before commiting
