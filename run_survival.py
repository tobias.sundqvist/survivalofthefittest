#
# main game file, run this file with python and the game will start
#

import os
import pygame
from survival_game import SurvivalGame
from ddqn_agent import DQNAgent
from survival_game import GameScreen
from survival_game import LevelFactory
from survival_game import ActionEnum
from survival_game import Helper

import numpy as np
import sys
import time
import argparse

parser = argparse.ArgumentParser(description='usage: python run_survival.py < -r0 optional> < -l1 optional >')
parser.add_argument("-r", type=int, help="run with -r0 to not render, only train model", default="1")
parser.add_argument("-l", type=int, help="run with -l1 to load stored model and not train, just see how the animal behaves", default="0")

args = parser.parse_args()
render = (args.r == 1)
loadModel = (args.l == 1)

#  Game screen should look like this
# |-----------------|
# |                 | game area is where animals are moving
# |  game (320x240) |
# |                 |
# |-----------------|
# |  info (320x48)  | info are is showing points etc
# |-----------------|

# game specific parameters
GAME_WIDTH = 320
GAME_HEIGHT = 240
GAME_INFO_SPACE = 48
GAME_TIME = 500 #frames
NUM_GAMES = 5000
BATCH_SIZE = 32

# Initialise pygame
os.environ["SDL_VIDEO_CENTERED"] = "1"
pygame.display.init()
pygame.font.init()
font = pygame.font.SysFont("Grobold", 20)

# Set up the display
pygame.display.set_caption("Survival of the fittest")
screen = pygame.display.set_mode((GAME_WIDTH, GAME_HEIGHT+GAME_INFO_SPACE))
gameScreen = GameScreen(screen, font, GAME_WIDTH,GAME_HEIGHT)

#Load the game
levels = [2]
lvlFact = LevelFactory(levels)
game = SurvivalGame(lvlFact)
lvl_num = 2

# environment contains the number of states and number of actions that can be done, the agent will handle the animals movement 
num_states, num_actions = game.getEnv()
animal_agent = DQNAgent(num_states, num_actions)

if loadModel:
    animal_agent.load("./animal.h5")
    animal_agent.setFullyTrained()
    random_action = 0
else:
    random_action = 1

# Init of variables for the start of the game
end_game = False
epsilon = 1

for num_game in range(NUM_GAMES):

    # reset state in the beginning of each game
    game.initLevel(lvl_num)
    walls = game.getWalls()
    grasses = game.getGrass()
    game.reset(GAME_WIDTH,GAME_HEIGHT)  
    state = game.getState()
    state = np.reshape(state, [1, num_states])
    player_action = ActionEnum.DO_NOTHING
    perform_player_action = False

    for time_t in range(GAME_TIME):
	# Decide action
        action = animal_agent.actUsingLargestTwoActions(state, random_action)
        
        # Advance the game to the next frame based on the action.
        next_state, reward, done = game.step(action)
        next_state = np.reshape(next_state, [1, num_states])

        # Remember the previous state, action, reward, and done
        animal_agent.remember(state, action, reward, next_state, done)

        # make next_state the new current state for the next frame.
        state = next_state
          
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
               end_game = True
            elif event.type == pygame.KEYDOWN:
                key_name = pygame.key.name(event.key)
                player_action = Helper.ConvertKeyToAction(key_name)
            elif event.type == pygame.KEYUP:
                perform_player_action = True

        if perform_player_action:
            if player_action != ActionEnum.DO_NOTHING:
                game.movePlayer(player_action)
            perform_player_action = False
 
        if render:
            time.sleep(0.1)
            animal = game.getAnimal()
            player = game.getPlayer()             
            gameScreen.updateText(game.getTime(),game.getPoints())
            gameScreen.draw()
            
            for wall in walls:
                pygame.draw.rect(screen, (255, 255, 255), wall.rect)
            for grass in grasses:
                pygame.draw.rect(screen, (0, 255, 0), grass.rect)

            pygame.draw.rect(screen, (255, 200, 0), animal.rect)
            pygame.draw.rect(screen, (0, 0, 255), player.rect)            
            pygame.display.flip()


        if game.shouldGameEnd():
            break
        if end_game:
            break

    # End of game -------------------
    if render:
        if not end_game:
            print("%s" %game.getWinner())
            while(True):
                print("Press space to end game")
                event = pygame.event.wait()
                if event.type == pygame.KEYDOWN:
                    key_name = pygame.key.name(event.key)
                    if key_name == "space":
                        break

            
    # train the animal for each game
    if len(animal_agent.memory) > BATCH_SIZE:
        epsilon = animal_agent.replay(BATCH_SIZE)
      
    # update target_model after every 20 games
    if num_game % 20 == 0:
        animal_agent.update_target_model()
        print("game num: %d (%d), epsilon: %f" %(num_game,NUM_GAMES, epsilon))

    print("Animal points received %d" %game.getAnimalPoints())
    
    if end_game:
        break

if not loadModel: #only save the model if it is not loaded
    animal_agent.save("./animal.h5")

print("Ending display")
pygame.display.quit()
print("Ending game")
pygame.quit()
