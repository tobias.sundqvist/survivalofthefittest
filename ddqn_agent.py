#This agent is inpsired by the Deep Q nerual network used for chess playing etc.
# The init parameters are used to set how the learning/exploration behaviour will vary over time.
#

import random
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from keras import backend as K

EPISODES = 5000
MAX_MEMORY = 2000 
MAX_DIFF_ACTION_VALUES = 0.95

class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=MAX_MEMORY)
        self.gamma = 0.95    # discount rate 
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01  #stops at this rate
        self.explore_step = 5000
        self.epsilon_decay = (self.epsilon - self.epsilon_min) / self.explore_step

        #self.epsilon_decay = 0.99 #decay for each learning phase
        self.learning_rate = 0.001 #learning rate
        self.model = self._build_model()
        self.target_model = self._build_model()
        self.update_target_model()

    def _huber_loss(self, target, prediction):
        #less sensitive to outliers in data than the squared error loss.
        # sqrt(1+error^2)-1
        error = prediction - target
        return K.mean(K.sqrt(1+K.square(error))-1, axis=-1)

    def _build_model(self):
        #build the model for the deep-Q learning
        model = Sequential()
        # input + output /2 -> (12 + 5)/2 -> 8
        model.add(Dense(8, input_dim=self.state_size, activation='relu'))  #24
        #model.add(Dense(12, activation='relu'))  #24
        #model.add(Dense(5, input_dim=self.state_size, activation='relu'))  #24
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss=self._huber_loss,
                      optimizer=Adam(lr=self.learning_rate))
	#model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
        return model

    def update_target_model(self):
        #Update the target model with the weights the model has at the moment
        # copy weights from model to target_model
        self.target_model.set_weights(self.model.get_weights())

    def remember(self, state, action, reward, next_state, done):
        #remember the states
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state,rand):
        #the agent makes an action, either random or chooses the best action depending on a prediction for what will be the best action knowing the input state.
        if rand == 1:
            if np.random.rand() <= self.epsilon:
                return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def actUsingLargestActions(self, state,rand):
        #the agent makes an action, either random or chooses the best action depending on a prediction for what will be the best action knowing the input state.
        #compare the largest action and choose between them that are 0.95 of largest
        if rand == 1:
            if np.random.rand() <= self.epsilon:
                return random.randrange(self.action_size)
        
        act_values = self.model.predict(state)
        largest_value = np.amax(act_values[0])
        iActList = list() #list of actions that has close value with the largest one
        i = 0
        for value in act_values[0]:
            if value / largest_value > MAX_DIFF_ACTION_VALUES:
                iActList.append(i)
            i += 1

        # Randomize between the chosen values
        rand_act_i = random.randrange(len(iActList))
        action = iActList[rand_act_i]
        return action

    def actUsingLargestTwoActions(self, state,rand):
        #the agent makes an action, either random or chooses the best action depending on a prediction for what will be the best action knowing the input state.
        # finds the two largest actions and chooses between those in the action is predicted.
        # depending on the prediction there will be higher probability to take an action with higher probability when choosing between the largest two probabilities
        if rand == 1:
            if np.random.rand() <= self.epsilon:
                return random.randrange(self.action_size)
        
        act_values = self.model.predict(state)
        #largest_value = np.amax(act_values[0])
        actions = np.sort(act_values[0])
        largest_prediction = actions[-1]
        second_largest_prediction = actions[-2]
        first_sec_prop = largest_prediction / (largest_prediction + second_largest_prediction)
        if  np.random.rand() < first_sec_prop:
            action = list(act_values[0]).index(largest_prediction)
        else:
            action = list(act_values[0]).index(second_largest_prediction)
        
        return action

    def replay(self, batch_size):
        #this is where the agent learns, it replays all actions performed in a batch and learns from it
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = self.model.predict(state)
            if done:
                target[0][action] = reward
            else:
                a = self.model.predict(next_state)[0]
                t = self.target_model.predict(next_state)[0]
                target[0][action] = reward + self.gamma * t[np.argmax(a)]
            self.model.fit(state, target, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon -= self.epsilon_decay
        return self.epsilon
    
    def load(self, name):
        #loads the trained model state from file
        self.model.load_weights(name)

    def save(self, name):
        #saves the model state in file
        self.model.save_weights(name)

    def setFullyTrained(self):
        #minimizes the epsilon which will make the agent to almost not take an random action
        self.epsilon = self.epsilon_min
