from survival_game import Animal
from survival_game import Grass
from survival_game import Wall
from survival_game import SurvivalGame
from survival_game import ActionEnum
from survival_game import LevelFactory
from survival_game import Player
from survival_game import Helper
from survival_game import GameScreen
from survival_game import GameBoard
from survival_game import ObjectEnum

from ddqn_agent import DQNAgent
import pygame
import numpy as np
import os
import unittest

class TestGrass(unittest.TestCase):
    def test_grass(self):
        pos = 1,2
        grass = Grass(pos)
        self.assertNotEqual(grass,None, "should not be None")

class TestWall(unittest.TestCase):
    def test_wall(self):
        pos = 1,2
        wall = Wall(pos)
        self.assertNotEqual(wall,None, "should not be None")

class TestPlayer(unittest.TestCase):
    def setUp(self):
        lvlFact = LevelFactory()
        level =  [
            "WWWW",
            "WPGW",
            "WGAW",
            "WWWW",
        ]
        lvlFact.addLevel(level,1)
        self.game = SurvivalGame(lvlFact)
        self.game.initLevel()     
        self.player = self.game.getPlayer()

    def test_move(self):
        dx,dy = 0,16
        self.player.move(dx,dy,self.game.game_board)
        x,y = self.player.rect.center
        self.assertEqual(x, 24, "should be 24")  #16*2+8 - 24
        self.assertEqual(y, 40, "should be 40")  #16*2+8 - 0


class TestHelper(unittest.TestCase):
    def test_ConvertKeyToAction(self):
        action = Helper.ConvertKeyToAction("left")
        self.assertEqual(action,ActionEnum.LEFT, "should be ActionEnum.LEFT")
        action = Helper.ConvertKeyToAction("right")
        self.assertEqual(action,ActionEnum.RIGHT, "should be ActionEnum.RIGHT")
        action = Helper.ConvertKeyToAction("up")
        self.assertEqual(action,ActionEnum.UP, "should be ActionEnum.UP")
        action = Helper.ConvertKeyToAction("down")
        self.assertEqual(action,ActionEnum.DOWN, "should be ActionEnum.DOWN")
        action = Helper.ConvertKeyToAction("unknown")
        self.assertEqual(action,ActionEnum.DO_NOTHING, "should be ActionEnum.DO_NOTHING")

    def test_convertToPos(self):
        center = 33,47
        x_coord,y_coord = Helper.convertPosToCoord(center)
        self.assertEqual(x_coord, 2 , "x should be 2")
        self.assertEqual(y_coord, 2 , "y should be 2")
        center = 1,238
        x_coord,y_coord = Helper.convertPosToCoord(center)
        self.assertEqual(x_coord, 0 , "x should be 0")
        self.assertEqual(y_coord, 14 , "y should be 14")

    def test_isMoveAction(self):
        didMove =Helper.isMoveAction(ActionEnum.UP)
        self.assertEqual(didMove, True, "should be True")
        didMove = Helper.isMoveAction(ActionEnum.DO_NOTHING)
        self.assertEqual(didMove, False, "should be False")

class TestAnimal(unittest.TestCase):
    def setUp(self):
        lvlFact = LevelFactory()
        level =  [
            "WWWW",
            "WPGW",
            "WGAW",
            "WWWW",
        ]
        lvlFact.addLevel(level,1)
        self.game = SurvivalGame(lvlFact)
        self.game.initLevel()     
        self.animal = self.game.getAnimal()

    def test_center(self):
        center_x,center_y = self.animal.getCenter()
        self.assertEqual(center_x, 40, "should be 40")  #16*2+8
        self.assertEqual(center_y, 40, "should be 40")  #16*2+8

    def test_move(self):
        dx = -16
        dy = 0
        self.animal.move(dx,dy,self.game.game_board)
        center_x,center_y = self.animal.getCenter()
        self.assertEqual(center_x, 24, "should be 24")  #16*2+8 - 24
        self.assertEqual(center_y, 40, "should be 40")  #16*2+8 - 0

    def test_getCollissionWall(self):
        collision = self.animal.getCollissionWall()
        self.assertEqual(collision, False, "should be false")
        dx = 16
        dy = 0
        self.animal.move(dx,dy,self.game.game_board)
        collision = self.animal.getCollissionWall()
        self.assertEqual(collision, True, "should be true")

    def test_getGrasEaten(self):
        eaten = self.animal.getGrasEaten()
        self.assertEqual(eaten, False, "should be false")
        dx = -16
        dy = 0
        self.animal.move(dx,dy,self.game.game_board)
        eaten = self.animal.getGrasEaten()
        self.assertEqual(eaten, True, "should be true")

class TestLevelFactory(unittest.TestCase):
    def test_addLevel(self):
        lvlFact = LevelFactory()
        num_levels = len(lvlFact.lvlDict)
        self.assertEqual(num_levels, 0, "should be 0")
        level =  [
            "WWWW",
            "WPGW",
            "WGAW",
            "WWWW",
        ]
        lvlFact.addLevel(level,1)
        num_levels = len(lvlFact.lvlDict)
        self.assertEqual(num_levels, 1, "should be 1")
        lvlFact.addLevel(level,2)
        num_levels = len(lvlFact.lvlDict)
        self.assertEqual(num_levels, 2, "should be 2")

    def test_getLevel(self):
        lvlFact = LevelFactory()
        level = lvlFact.getLevel(1)
        self.assertEqual(level, None, "should be None")
        level =  [
            "WWWW",
            "WPGW",
            "WGAW",
            "WWWW",
        ]
        lvlFact.addLevel(level,1)
        level = lvlFact.getLevel(1)
        self.assertEqual(len(level[0]), 4 , "should be 4")
        self.assertEqual(len(level[1]), 4 , "should be 4")

class TestGame(unittest.TestCase):
    def setUp(self):
        lvlFact = LevelFactory()
        level =  [
            "WWWW",
            "WPGW",
            "WGAW",
            "WWWW",
        ]
        lvlFact.addLevel(level,1)
        self.game = SurvivalGame(lvlFact)

    def test_getEnv(self):
        env = self.game.getEnv()
        num_states,num_actions = env
        self.assertEqual(num_states, 12, "should be 12 states")
        self.assertEqual(num_actions, 5, "should be 5")

    def test_initLevel(self):
        animal = self.game.getAnimal()
        self.assertEqual(animal, None , "should be None")
        self.game.initLevel()
        animal = self.game.getAnimal()
        self.assertNotEqual(animal, None , "should not be None")
        walls = self.game.getWalls()
        numWalls = len(walls)
        self.assertEqual(numWalls, 12 , "should be 12")
        grass = self.game.getGrass()
        numGrass = len(grass)
        self.assertEqual(numGrass, 2 , "should be 2")
        p1 =self.game.getPlayer()
        self.assertNotEqual(p1, None , "should not be None")

    def test_reset(self):
        self.game.reset(64,64)
        animal = self.game.getAnimal()
        self.assertEqual(animal, None , "should be None")
        animal_points, player_points = self.game.getPoints()
        self.assertEqual(animal_points, 0 , "should be 0")
        self.assertEqual(player_points, 0 , "should be 0")
        time = self.game.getTime()
        self.assertEqual(time, 0 , "should be 0")

    def test_step(self):
        # the states are received as following:
        #   1
        # 2 x 3
        #   4
        # where x is the position of the animal
        #walls are received in the first 4 indexes then 4 indexes with grass
        # then 4 grass square states are received
        #|  0  |  1  |
        #|-----|-----|
        #|  2  |  3  |
        #they contain number of grass in each square of the board
        self.game.initLevel()

        self.game.reset(64,64)
        animal_points, player_points = self.game.getPoints()
        self.assertEqual(animal_points, 0 , "should be 0")
        self.assertEqual(player_points, 0 , "should be 0")
        time = self.game.getTime()
        self.assertEqual(time, 0 , "should be 0")

        state, reward, gameOver = self.game.step(ActionEnum.DO_NOTHING)
        rightObj = state[2] #Check WALL first
        leftObj = state[1]
        upObj = state[0]
        downObj = state[3]
        self.assertEqual(rightObj, 1 , "should be wall there")
        self.assertEqual(leftObj, 0 , "should be none there")
        self.assertEqual(upObj, 0 , "should be none there")
        self.assertEqual(downObj, 1 , "should be wall there")
        rightObj = state[6] #check GRASS
        leftObj = state[5]
        upObj = state[4]
        downObj = state[7]
        self.assertEqual(rightObj, 0 , "should be none there")
        self.assertEqual(leftObj, 1 , "should be grass there")
        self.assertEqual(upObj, 1 , "should be grass there")
        self.assertEqual(downObj, 0 , "should be none there")
        square0 = state[8]
        square1 = state[9]
        square2 = state[10]
        square3 = state[11]
        self.assertEqual(square0, 0 , "should be 0 grass there")
        self.assertEqual(square1, 0.5 , "should be 1 grass there (total 2) so 1/2")
        self.assertEqual(square2, 0.5 , "should be 1 grass there (totalt 2) so 1/2")
        self.assertEqual(square3, 0 , "should be 0 grass there")
        self.assertEqual(reward, 0 , "should be 0")
        self.assertEqual(gameOver, 0 , "should be 0")        
        animal_points, player_points = self.game.getPoints()
        self.assertEqual(animal_points, 0 , "should be 0")
        self.assertEqual(player_points, 0 , "should be 0")
        time = self.game.getTime()
        self.assertEqual(time, 1 , "should be 1")

    def test_reward(self):
        self.game.initLevel()

        self.game.reset(64,64)
        reward = self.game.getReward()
        self.assertEqual(reward, 0 , "should be 0")
        self.game.step(ActionEnum.RIGHT) #causes collision (animal is in bottom right corner)
        reward = self.game.getReward() #get no point at all for not moving
        self.assertEqual(reward, -10 , "should be -10")
        self.game.step(ActionEnum.LEFT) #causes collision with grass
        reward = self.game.getReward()
        self.assertEqual(reward, 5 , "should be 5")

    def test_getObjectPresenceInSurroundingSquares(self):
        self.game.initLevel()
        coord = 1,1
        found_objects = self.game.game_board.getObjectPresenceInSurroundingSquares(coord)
        #list of 9 wall states and then 9 grass states
        num_list = len(found_objects)
        real_obj = [1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0 ]
        self.assertEqual(num_list, 18 , "should be 18 states in list")
        i = 0
        for obj_found,obj in zip(found_objects,real_obj):
            if obj == obj_found:
                objFound = True
            else:
                objFound = False
            self.assertTrue(objFound , "should be obj %d in this pos i:%d" %(obj,i))
            i += 1

    def test_movePlayer(self):
        self.game.initLevel()
        self.game.movePlayer(ActionEnum.DOWN)
        p1 = self.game.getPlayer()
        pos = p1.rect.center
        x,y = pos
        #was at center 8+16,8+16 = 24,24, moving 16 DOWN -> 40
        self.assertEqual(x, 24 , "should be 24")
        self.assertEqual(y, 40 , "should be 40")

        self.game.movePlayer(ActionEnum.RIGHT)
        p1 = self.game.getPlayer()
        pos = p1.rect.center
        x,y = pos
        #was at center 24,40, moving 16 RIGHT -> 40
        self.assertEqual(x, 40 , "should be 40")
        self.assertEqual(y, 40 , "should be 40")

    def test_moveAnimal(self):
        self.game.initLevel()
        animal = self.game.getAnimal()
        self.game.moveAnimal(ActionEnum.DOWN)
        pos = animal.rect.center
        #was at center 8+16*2,8+16*2 = 40,40, moving DOWN should collide and not move
        self.assertEqual(pos, (40,40) , "should be 40,40")
        self.game.moveAnimal(ActionEnum.UP)
        pos = animal.rect.center
        #was at center 8+16*2,8+16*2 = 40,40, moving UP should decrease y with 16
        self.assertEqual(pos, (40,24) , "should be 40,24")

    def test_getState(self):
        #first 4 states are walls, next 4 are grass, next 4 are average num grass in board squares
        self.game.initLevel()
        states = self.game.getState()
        #print(states)
        walls = [0,0,1,1]
        grass = [1,1,0,0]
        num_grass = [0.0,0.5,0.5,0.0]
        self.assertEqual(states, walls+grass+num_grass , "should be 0,0,1,1,1,1,0,0,0.0,0.5,0.5,0.0")

    def test_postGame(self):
        # should just print out on screen the number of points
        self.game.postGame()

    def test_getAnimalPoints(self):
        animal_points = self.game.getAnimalPoints()
        self.assertEqual(animal_points, 0 , "should be 0")

    def test_getWinner(self):
        winner = self.game.getWinner()
        winnerShouldBe = "A:0 - P:0, its a tie between the animal and player"
        self.assertEqual(winner, winnerShouldBe , "should be equal")

    def test_shouldGameEnd(self):
        self.game.initLevel()
        shouldGameEnd = self.game.shouldGameEnd()
        self.assertEqual(shouldGameEnd, False , "should be equal")
        self.game.animal_points = 2
        shouldGameEnd = self.game.shouldGameEnd()
        self.assertEqual(shouldGameEnd, True , "should be equal")
        self.game.animal_points = 0
        self.game.player_points = 2
        shouldGameEnd = self.game.shouldGameEnd()
        self.assertEqual(shouldGameEnd, True , "should be equal")
        self.game.game_board.grassDict.clear()
        self.game.player_points = 0
        shouldGameEnd = self.game.shouldGameEnd()
        self.assertEqual(shouldGameEnd, True , "should be equal")



class TestDdqnAgent(unittest.TestCase):
    def setUp(self):
        self.num_states = 2 #easier to test 
        self.num_actions = 5
        self.agent = DQNAgent(self.num_states, self.num_actions)

    def test_init(self):
        self.assertNotEqual(self.agent, None, "should not be None")

    def test_act(self):
        states = 1,2 
        states = np.reshape(states, [1, self.num_states])

        random_action = 0
        action = self.agent.act(states, random_action)
        self.assertTrue(0 <= action <= self.num_actions, "should be between 0..5")

        random_action = 1
        action = self.agent.act(states, random_action)
        self.assertTrue(0 <= action <= self.num_actions, "should be between 0..5")

    def test_actUsingLargestActions(self):
        states = 1,2 
        states = np.reshape(states, [1, self.num_states])

        random_action = 0
        action = self.agent.actUsingLargestActions(states, random_action)
        self.assertTrue(0 <= action <= self.num_actions, "should be between 0..5")

        random_action = 1
        action = self.agent.act(states, random_action)
        self.assertTrue(0 <= action <= self.num_actions, "should be between 0..5")

    def test_actUsingLargestTwoActions(self):
        states = 1,2 
        states = np.reshape(states, [1, self.num_states])

        random_action = 0
        action = self.agent.actUsingLargestTwoActions(states, random_action)
        self.assertTrue(0 <= action <= self.num_actions, "should be between 0..5")

        random_action = 1
        action = self.agent.actUsingLargestTwoActions(states, random_action)
        self.assertTrue(0 <= action <= self.num_actions, "should be between 0..5")

    def test_remember_act(self):
        state = 1,1
        action = ActionEnum.RIGHT
        reward = 0
        next_state = 1,2
        gameOver = 0
        state = np.reshape(state, [1, self.num_states])
        next_state = np.reshape(next_state, [1, self.num_states])   
        
        self.agent.remember(state, action, reward, next_state, gameOver)
        epsilon = self.agent.replay(1)
        self.assertEqual(epsilon, 0.999802 , "should be 0.99")

        self.agent.setFullyTrained()
        epsilon = self.agent.replay(1)
        self.assertEqual(epsilon, 0.01 , "should be 0.01")

    def test_save_load(self):
        self.agent.save("unittest.h5")
        self.agent.load("unittest.h5")
        os.remove("unittest.h5")
        #exception should occur if save or load is not working

class TestGameScreen(unittest.TestCase):
    def setUp(self):
        pygame.font.init()
        self.font = pygame.font.SysFont("Grobold", 20)
        self.screen = pygame.display.set_mode((64, 64+32))
        self.game_screen = GameScreen(self.screen, self.font, 64, 64)

    def test_draw(self):
        self.game_screen.draw()
        #just check so that function works, no possible assert to check

    def test_updateText(self):
        self.game_screen.updateText(100,(10,1))
        #just check so that function works, no possible assert to check

    def tearDown(self):
        pygame.font.quit()
        pygame.quit()

class TestGameBoard(unittest.TestCase):
    def setUp(self):
        self.game_board = GameBoard(4,4)
        level =  [
            "WWWW",
            "WPGW",
            "WGAW",
            "WWWW",
        ]
        self.game_board.addLevelObjects(level)

    def test_checkWhichSquare(self):
        #|  0  |  1  |
        #|-----|-----|
        #|  2  |  3  |
        x_pos,y_pos = 1,1
        whichSquare = self.game_board.checkWhichSquare(x_pos,y_pos)
        self.assertEqual(whichSquare, 0 , "should be 0")
        x_pos,y_pos = 3,3
        whichSquare = self.game_board.checkWhichSquare(x_pos,y_pos)
        self.assertEqual(whichSquare, 3 , "should be 3")

    def test_getAnimal(self):
        animal = self.game_board.getAnimal()
        self.assertNotEqual(animal, None  , "should not be None")

    def test_getPlayer(self):
        player = self.game_board.getPlayer()
        self.assertNotEqual(player, None  , "should not be None")

    def test_removeObject(self):
        #grass is in coord 1,2
        coord = 1,2
        grassPresent = self.game_board.checkIfObjectIsInSquare(ObjectEnum.GRASS,coord)
        self.assertEqual(grassPresent, 1 , "should be 1")
        self.game_board.removeObject(ObjectEnum.GRASS,coord)
        grassPresent = self.game_board.checkIfObjectIsInSquare(ObjectEnum.GRASS,coord)
        self.assertEqual(grassPresent, 0 , "should be 0")

    def test_getObjectPresenceInMoveDir(self):
        #   1   
        # 2 x 3  coordinate is in pos x 
        #   4
        #returns 8 objects, first 4 is walls, next 4 is grass
        coord = 1,1
        objects = self.game_board.getObjectPresenceInMoveDir(coord)
        self.assertEqual(objects[0], 1 , "should be 1") #
        self.assertEqual(objects[1], 1 , "should be 1") #
        self.assertEqual(objects[2], 0 , "should be 0") #
        self.assertEqual(objects[3], 0 , "should be 0") #
        self.assertEqual(objects[4], 0 , "should be 0") #
        self.assertEqual(objects[5], 0 , "should be 0") #
        self.assertEqual(objects[6], 1 , "should be 1") #
        self.assertEqual(objects[7], 1 , "should be 1") #

    def test_getObjectsInBoardSquare(self):
        coord = 1,2 #should be grass there, returns objects is a scaler combining all objects
        objects = self.game_board.getObjectsInBoardSquare(coord)
        self.assertEqual(objects, ObjectEnum.GRASS , "should be Grass") 
        coord = 0,0 #should be wall there
        objects = self.game_board.getObjectsInBoardSquare(coord)
        self.assertEqual(objects, ObjectEnum.WALL , "should be Wall") 
        coord = 1,1 #should be nothing there (animal and player does not count)
        objects = self.game_board.getObjectsInBoardSquare(coord)
        self.assertEqual(objects, ObjectEnum.NONE , "should be None") 

    def test_getWalls(self):
        walls = self.game_board.getWalls()
        self.assertEqual(len(walls), 12 , "should be 12 walls") 

    def test_getGrass(self):
        grass = self.game_board.getGrass()
        self.assertEqual(len(grass), 2 , "should be 2 grass") 

    def test_getGrassInSquares(self):
        #|  0  |  1  |
        #|-----|-----|
        #|  2  |  3  |
        num_grasses = self.game_board.getGrassInSquares()
        self.assertEqual(len(num_grasses), 4 , "should be 4 squares with grass")
        self.assertEqual(num_grasses[0], 0 , "should be 0 gras in this square") 
        self.assertEqual(num_grasses[1], 1 , "should be 1 gras in this square") 
        self.assertEqual(num_grasses[2], 1 , "should be 1 gras in this square") 
        self.assertEqual(num_grasses[3], 0 , "should be 0 gras in this square") 

    def test_getNumOfGrass(self):
        num_grass = self.game_board.getNumOfGrass()
        self.assertEqual(num_grass, 2 , "should be 2")

if __name__ == '__main__':
    unittest.main()
