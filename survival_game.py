import pygame
from enum import IntEnum, unique
import numpy as np

WALL_SIZE = 16
GRASS_SIZE = 16
ANIMAL_SIZE = 16
PLAYER_SIZE = 16
NUM_ANIMAL_ACTIONS = 5
SQUARE_SIZE = 16

GRASS_POINT = 1

ANIMAL_STEP = 16
PLAYER_STEP = 16
REWARD_GRASS = 5
REWARD_COL_WALL = -10
REWARD_ANIMAL_MOVED = 1

all_levels = [[
"WWWWWWWWWWWWWWWWWWWW",
"WA                 W",
"W          G       W",
"W   G              W",
"W               G  W",
"W       G          W",
"W                  W",
"W  G         G     W",
"W                  W",
"W                  W",
"W    G             W",
"W             G    W",
"W  G               W",
"W                 PW",
"WWWWWWWWWWWWWWWWWWWW",
],
[
"WWWWWWWWWWWWWWWWWWWW",
"WAGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGGW",
"WGGGGGGGGGGGGGGGGGPW",
"WWWWWWWWWWWWWWWWWWWW",
]]

# Enum used for actions the agent can take
@unique
class ActionEnum(IntEnum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3
    DO_NOTHING = 4

# Objects in the game
@unique
class ObjectEnum(IntEnum):
    NONE = 0
    WALL = 1
    GRASS = 2
    ANIMAL = 4
    PLAYER = 8

class Helper:

    @staticmethod
    def ConvertKeyToAction(key):
        #converts an key from the keyboard to an Enum
        action = ActionEnum.DO_NOTHING
        if key == "left":
            action = ActionEnum.LEFT
        elif key == "right":
            action = ActionEnum.RIGHT
        elif key == "up":
            action = ActionEnum.UP
        elif key == "down":
            action = ActionEnum.DOWN
        return action

    @staticmethod
    def convertPosToCoord(center):
        # x,y pos is transfromed to grid coordinate on board
        # Input: center - and x,y pixel position of an object
        # returns: and x/y coordinate of the game board
        #---|------|-  as long as middle coordinate is in within square
        #   |    x |   then it is in the square
        #---|------|-
        x,y = center
        x_coord = int(x / SQUARE_SIZE)
        y_coord = int(y / SQUARE_SIZE)
        return x_coord, y_coord

    @staticmethod    
    def isMoveAction(action):
        # Checks if the action is an move action or not
        # Input: action - movement action
        objectMoved = False
        if action != ActionEnum.DO_NOTHING:
            objectMoved = True
        return objectMoved

# Class to hold the variables for the game screen
# This Class is tested by the run_survival.py file
class GameScreen(object):
    def __init__(self, screen, font, game_width, game_height):
        #Initialize game screen variables
        #Input: screen - pygame display
        #Input font - pygame font
        #Input game_width/height - size of the game screen
        self.screen = screen
        self.font = font

        #position of the time and point counter
        self.count_pos_left = 5  
        self.count_pos_top = game_height + 5
        self.points_pos_left = 5 
        self.points_pos_top = game_height + 5 + 12

        #rectangles containing the time and point counters that are displayed on the screen
        self.counting_text = self.font.render("T:0", 1, (255,255,255))
        self.counting_rect = self.counting_text.get_rect(center = self.screen.get_rect().center)
        self.point_text = self.font.render("Points Animal:0, Player points:0", 1, (255,255,255))
        self.point_rect = self.point_text.get_rect(left = self.points_pos_left, top= self.points_pos_top)

    def draw(self):
        #clears the screen and draws the time and point
        self.screen.fill((0, 0, 0))
        self.screen.blit(self.counting_text, self.counting_rect)
        self.screen.blit(self.point_text, self.point_rect)

    def updateText(self,time, points):
        #Updates the time and point text
        #Input: time - time counter for the game
        #Input: points - number of points received for the animal
        animal_points, player_points = points
        self.counting_text = self.font.render("T:%s" %str(time), 1, (255,255,255))
        self.point_text = self.font.render("Animal Points:%d, Player Points:%d" %(animal_points, player_points), 1, (255,255,255))
        self.counting_rect = self.counting_text.get_rect(left = self.count_pos_left, top= self.count_pos_top)
        self.point_rect = self.point_text.get_rect(left = self.points_pos_left, top= self.points_pos_top)

# Class to hold a wall rect
class Wall(object):
    def __init__(self, pos):
        #Input pos is a x,y pixel coordinate of the center of the Wall
        self.rect = pygame.Rect(pos[0], pos[1], WALL_SIZE, WALL_SIZE)

# Class to hold a grass rect
class Grass(object):
    def __init__(self, pos):
        #Input pos is a x,y pixel coordinate of the center of the Grass       
        self.rect = pygame.Rect(pos[0], pos[1], GRASS_SIZE, GRASS_SIZE)

# Class for the animal
class Animal(object):
    def __init__(self,x,y):
        #Input x,y is x,y pixel coordinates of the center of the Animal
        self.rect = pygame.Rect(x, y, ANIMAL_SIZE, ANIMAL_SIZE)
        self.dx = 0
        self.dy = 0
        self.collideWall = 0 # 0 = no collision
        self.grasEaten = False

    def getCenter(self):
        #returns the center of the object
        return self.rect.center
    
    def move(self, dx, dy, game_board):
        # moves the object
        # Input: dx/dy - number of pixels object moved in x/y (can be +- any number)
        # Input: walls/grass - all wall and grass objects in the game
        self.dx = dx
        self.dy = dy
        self.collideWall = False 
        self.grasEaten = False

        if dx != 0 or dy != 0:
            self.rect.x += dx
            self.rect.y += dy
            
            #check if anything is found in the square we moved to
            coord = Helper.convertPosToCoord((self.rect.x,self.rect.y)) 
            foundObj = game_board.getObjectsInBoardSquare(coord)

            if((foundObj & ObjectEnum.WALL) != 0):
                #collision with wall, move back position since we cannot go through walls
                self.rect.x -= dx
                self.rect.y -= dy
                self.collideWall = True
            elif((foundObj & ObjectEnum.GRASS) != 0):
                self.grasEaten = True
                game_board.removeObject(ObjectEnum.GRASS, coord)

    def getCollissionWall(self):
        # returns 1 if object collided with wall the last movement, otherwise 0
        return self.collideWall

    def getGrasEaten(self):
        # returns True if animal has eaten gras during the last move
        return self.grasEaten

# Class for the player
class Player(object):
    def __init__(self,x,y):
        #Input x,y is x,y pixel coordinates of the center of the Player
        self.rect = pygame.Rect(x, y, PLAYER_SIZE, PLAYER_SIZE)
        self.dx = 0
        self.dy = 0
        self.grasEaten = False

    def move(self, dx, dy, game_board):
        # moves the object
        # Input: dx/dy - number of pixels object moved in x/y (can be +- any number)
        # Input: walls/grass - all wall and grass objects in the game
        self.dx = dx
        self.dy = dy
        self.rect.x += dx
        self.rect.y += dy
        self.grasEaten = False

        #check if anything is found in the square we moved to
        coord = Helper.convertPosToCoord((self.rect.x,self.rect.y)) 
        foundObj = game_board.getObjectsInBoardSquare(coord)

        if((foundObj & ObjectEnum.WALL) != 0):
            #collision with wall, move back position since we cannot go through walls
            self.rect.x -= dx
            self.rect.y -= dy
        elif((foundObj & ObjectEnum.GRASS) != 0):
                self.grasEaten = True
                game_board.removeObject(ObjectEnum.GRASS, coord)

    def getGrasEaten(self):
        # returns True if the player has eaten gras during the last move
        return self.grasEaten



# Class that holds all the levels, use this to get a level to the game
class LevelFactory(object):
    def __init__(self, levels=None):
        #Input level can be a list of numbers of the levels to setup
        # W = Wall, G = Grass
        self.lvlDict = dict()
        if levels != None:
            for lvl_num in levels:
                level = "level"+str(lvl_num)
                lvl_index = lvl_num - 1
                print("adding level: %s" %level)
                self.addLevel(all_levels[lvl_index], lvl_num)

    def addLevel(self, level, num):
        #adds a level in the dictionary
        #Input: level - an Ascci list of objects that represenats the game board
        #Input: num - the level number
        self.lvlDict[num] = level

    def getLevel(self, lvl_num):
        # returns a level
        # Input: lvl_num - the level number
        if lvl_num in self.lvlDict:
            return self.lvlDict[lvl_num]
        else:
            return None

class GameBoard(object):
    def __init__(self,board_length_x,board_length_y):
        #Input board_length_x,board_length_y is the number of objects with size 16x16 the board can have in lengt x and y
        self.board_length_x = board_length_x
        self.board_length_y = board_length_y
        self.game_board = np.zeros((board_length_x, board_length_y), dtype=int)
        self.walls = []
        self.grass = []
        self.player = None
        self.animal = None
        self.grassDict = dict()
        self.wallDict = dict()
        self.numGrassInSqares = [0] * 4
        self.total_game_points = 0
    
    def checkWhichSquare(self,x_pos,y_pos):
        #Input x_pos,y_pos is the coordinates we should check
        #returns which of the below squares that the coordinate is in
        #|  0  |  1  |
        #|-----|-----|
        #|  2  |  3  |
        square = 0
        if x_pos < self.board_length_x/2:
            if y_pos < self.board_length_y/2:
                square = 0
            else:
                square = 2
        else:
            if y_pos < self.board_length_y/2:
                square = 1
            else:
                square = 3
        return square

    def addLevelObjects(self,level):
        #Input level - ascii 0objects of an level, W = WALL, G = Grass, A = Animal, P = Player " " = Empty
        x = y = 0
        x_pos = y_pos = 0

        for row in level:
            for col in row:
              #left, top, width, height 
                if col == "W":
                    self.wallDict[x_pos,y_pos] = Wall((x,y)) 
                    self.game_board[x_pos][y_pos] += ObjectEnum.WALL
                if col == "G":
                    self.grassDict[x_pos,y_pos] = Grass((x,y))
                    self.game_board[x_pos][y_pos] += ObjectEnum.GRASS
                    i_square = self.checkWhichSquare(x_pos,y_pos)
                    self.numGrassInSqares[i_square] += 1
                if col == "A":
                    self.animal = Animal(x, y)
                if col == "P":
                    self.player = Player(x, y)
                else:
                    self.game_board[x_pos][y_pos] += ObjectEnum.NONE
                x += SQUARE_SIZE
                x_pos += 1
            y += SQUARE_SIZE
            y_pos += 1
            x = 0
            x_pos = 0

        self.total_game_points = len(self.grassDict) * GRASS_POINT

    def getAnimal(self):
        return self.animal

    def getPlayer(self):
        return self.player

    def removeObject(self, obj, coord):
        #Removes an object in the coord given
        x_coord, y_coord = coord
        #safety check
        if(self.getObjectsInBoardSquare(coord) > 0):
            if obj == ObjectEnum.GRASS:  #only object that is allowed to be removed for the moment
                del self.grassDict[x_coord,y_coord]
                self.game_board[x_coord][y_coord] -= obj
                i_square = self.checkWhichSquare(x_coord,y_coord)
                self.numGrassInSqares[i_square] -= 1

    def getObjectPresenceInSurroundingSquares(self,coord):
        # 1 2 3  loops through 9 squares to look for different kind of objects
        # 4 5 6  coordinate is in pos 5 
        # 7 8 9
        # Input: coord - game board coordinate
        
        x,y = coord
        objects = np.zeros((9*2,), dtype=int) 
        index = 0

        for j in range(y-1, y+2):
            for i in range(x-1, x+2):
                foundObj = self.getObjectsInBoardSquare((i,j))
                if (foundObj & ObjectEnum.WALL) != 0:
                    objects[index] = 1
                if (foundObj & ObjectEnum.GRASS) != 0:
                    objects[9 + index] = 1
                index += 1
        
        return objects

    def checkIfObjectIsInSquare(self,obj, coord):
        # Checks if the object is present in the coordinate
        foundObj = self.getObjectsInBoardSquare(coord)
        if (foundObj & obj) != 0:
            return 1
        else:
            return 0

    def getObjectPresenceInMoveDir(self,coord):
        #   1   looks in the squares that are in left,right, up and below for different kind of objects
        # 2 x 3  coordinate is in pos x 
        #   4 
        # Input: coord - game board coordinate
        # Returns an list of 4x2 integers 
        x,y = coord
        index = 0
        coords = [(x,y-1), (x-1,y),(x+1,y),(x,y+1)]
        num_coord = len(coords)
#        objects = np.zeros((num_coord*2,), dtype=int) 
        objects = [0] * (num_coord*2)

        for coord in coords:
            objects[index] = self.checkIfObjectIsInSquare(ObjectEnum.WALL, coord)
            objects[index + num_coord] = self.checkIfObjectIsInSquare(ObjectEnum.GRASS, coord)
            index += 1
        return objects

    def getObjectsInBoardSquare(self,coord):
        # returns the object value of the game board coordinate
        # Input: coord - an x,y game board coordinate
        return self.game_board[coord]

    def getWalls(self):
        return self.wallDict.values()

    def getGrass(self):
        return self.grassDict.values()

    def getGrassInSquares(self):
        return self.numGrassInSqares

    def getNumOfGrass(self):
        return len(self.grassDict)

    def getTotalGamePoints(self):
        return self.total_game_points

# Class that holds all instances of objects in the game
class SurvivalGame(object):
    def __init__(self, lvlFact):
        #Input lvlFactory that will contain the levels
        self.animal_points = 0
        self.player_points = 0
        self.animal = None
        self.player = None
        self.reward = 0
        self.time = 0 #time is a counter in how many steps the game has run
        self.game_board = None
        self.lvlFact = lvlFact

    def getEnv(self):
        # returns the number of states and actions that can be taken each turn
        number_of_states = 12 #the state is now the top,left,right and bottom of the animal which is 4 squares and there can be either wall or grass in these squares so 4*2 states. Then 4 extra states is added that describes the number of grass in each square of the board, top_left,top_right,bottom_lef,bottom_right
        number_of_actions = NUM_ANIMAL_ACTIONS  # animal can move up, down, left, right, or do nothing
        return number_of_states, number_of_actions

    def initLevel(self, lvl_num=1):
        # adds all instances of objects in the game board using the level
        # Input: lvl_num - the level number that should be used
        level = self.lvlFact.getLevel(lvl_num)
        board_length_x = len(level[0])
        board_length_y = len(level)
        self.game_board = GameBoard(board_length_x, board_length_y)
        self.game_board.addLevelObjects(level)
        self.player = self.game_board.getPlayer()
        self.animal = self.game_board.getAnimal()

    def reset(self, game_width,game_height):
        # resets all points, should be run after init
        self.animal_points = 0
        self.player_points = 0
        self.game_width = game_width
        self.game_height = game_height
        self.gameOver = 0
        self.time = 0

    def getState(self):
        # returns the state of the game
        x,y = self.animal.getCenter()
        coord = Helper.convertPosToCoord((x,y))
        state_dir = self.game_board.getObjectPresenceInMoveDir(coord)
        state_grass = self.game_board.getGrassInSquares()
        num_grass  = self.game_board.getNumOfGrass()
        if num_grass > 0:
            state_average_grass = [x / num_grass for x in state_grass]
        else:
            state_average_grass = [0,0,0,0]
        return state_dir + state_average_grass

    def getWalls(self):
        # returns all wall objects in the game
        return self.game_board.getWalls()

    def getGrass(self):
        # returns all grass objects in the game
        return self.game_board.getGrass()

    def getAnimal(self):
        # returns the animal object
        return self.animal

    def getPlayer(self):
        # returns the player object
        return self.player

    def postGame(self):
        # things that should occur after each game
        print("point received: %d" % self.animal_points)

    def getPoints(self):
        # returns the number of points
        return self.animal_points, self.player_points

    def getAnimalPoints(self):
        return self.animal_points

    def getWinner(self):
        retStr = "A:%d - P:%d, its a tie between the animal and player" %(self.animal_points,self.player_points)
        if self.animal_points > self.player_points:
            retStr = "A:%d - P:%d, Animal wins" %(self.animal_points,self.player_points)
        elif self.animal_points < self.player_points:
            retStr = "A:%d - P:%d, Player wins" %(self.animal_points,self.player_points)
        return retStr

    def getReward(self):
        # returns the reward received for the latest step (mainly used for testing at the moment)
        return self.reward

    def shouldGameEnd(self):
        gameShouldEnd = False
        halfOfGamePoints = self.game_board.getTotalGamePoints() / 2
        if self.animal_points > halfOfGamePoints:
            gameShouldEnd = True
        elif self.player_points > halfOfGamePoints:
            gameShouldEnd = True
        elif self.game_board.getNumOfGrass() == 0:
            gameShouldEnd = True
        return gameShouldEnd

    def moveAnimal(self, action):
        # move the animal in the game
        # Input: action - movement of the Animal
        if action == ActionEnum.UP:
            self.animal.move(0, -ANIMAL_STEP, self.game_board)
        elif action == ActionEnum.RIGHT:
            self.animal.move(ANIMAL_STEP, 0, self.game_board)
        elif action == ActionEnum.DOWN:
            self.animal.move(0, ANIMAL_STEP, self.game_board)
        elif action == ActionEnum.LEFT:
            self.animal.move(-ANIMAL_STEP, 0, self.game_board)
        else: #action == ActionEnum.DO_NOTHING
            self.animal.move(0, 0, self.game_board)

    def movePlayer(self,action):
        # move the player in the game
        # Input: action - movement of the Player
        if action == ActionEnum.UP:
            self.player.move(0, -ANIMAL_STEP, self.game_board)
        elif action == ActionEnum.RIGHT:
            self.player.move(ANIMAL_STEP, 0, self.game_board)
        elif action == ActionEnum.DOWN:
            self.player.move(0, ANIMAL_STEP, self.game_board)
        elif action == ActionEnum.LEFT:
            self.player.move(-ANIMAL_STEP, 0, self.game_board)
        
        if  self.player.getGrasEaten():
            self.player_points += GRASS_POINT


    def getTime(self):
        # Returns the time in the game
        return self.time
    
    def step(self,action):
        # performs one step in the game based on an action of an agent
        self.moveAnimal(action) 
        self.reward = 0

        #if self.isMoveAction(action):
        #    self.reward += REWARD_ANIMAL_MOVED 
	
        if self.animal.getCollissionWall():
            self.reward += REWARD_COL_WALL 

        if  self.animal.getGrasEaten():
            self.reward += REWARD_GRASS
            self.animal_points += GRASS_POINT

        self.time += 1

        return self.getState(),self.reward, self.gameOver
